const Router = require('../util/router');
const userController = require("../controllers/userController.js");

function subscribeUserRouter(server) {
  const router = new Router(server, '/user');

  router.get('', (req, res)  => userController.getUsers(req, res) );
  router.post('', (req, res)  => userController.addUser(req, res) );
  router.delete('', (req, res)  => userController.removeUser(req, res) );
  router.options('', (req, res)  => userController.optionsUser(req, res) );
}
module.exports = {
  subscribeUserRouter
}
