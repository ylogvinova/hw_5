const Router = require('../util/router');
const StatusController = require('../controllers/StatusController')

function subscribeStatusRoutes(server) {
  const router = new Router(server, '/status');
  router.get('/healthcheck',(req, res) => StatusController.healthCheck(req, res));
}

module.exports = {
  subscribeStatusRoutes
};
