let userList = [
  { id: 0, name: 'Dima', age: 25 },
  { id: 1, name: 'Vasya', age: 17 },
  { id: 2, name: 'Petya', age: 213 },
  { id: 3, name: 'Sasha', age: 32 },
  { id: 4, name: 'Kolya', age: 54 },
  { id: 5, name: 'Igor', age: 56 },
  { id: 6, name: 'Ura', age: 23 },
  { id: 7, name: 'Vasya', age: 45 }
];

function getUsers (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.end(JSON.stringify(userList));
}

function addUser (req, res) {
  const newUser = JSON.parse(req.body)
  res.setHeader('Content-Type', 'application/json')
  userList.push(newUser)
  res.end(JSON.stringify(newUser))
}

function removeUser (req, res) {
  const deleteUser = +req.body //make number
  userList = userList.filter(user => user.id !== deleteUser);
  res.setHeader('Content-Type', 'application/json')
  res.end(JSON.stringify(userList));
}

function optionsUser (req, res) {
  res.setHeader('Content-Type', 'application/json')
  res.end();
}

module.exports = {
  getUsers,
  addUser,
  removeUser,
  optionsUser
}
