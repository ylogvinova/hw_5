function healthCheck (req, res) {
  res.statusCode = 200;
  res.end('ok');
}

module.exports = {
  healthCheck,
}
